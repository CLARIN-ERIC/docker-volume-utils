#!/bin/bash

set -e

function help() {
    echo "Command help"
    exit 1
}

function backup() {
    echo "Backup, name=${2}"

    if [ "$1" == "copy" ]; then
        mkdir "/output/${NAME}"
        cp -r "/input" "/output/${NAME}"
    elif [ "$1"  == "archive" ]; then
        tar -cvf "/output/backup_${2}.tar" -C "/input" "."
    else
        echo "Invalid mode: ${1}. Expected copy or archive".
        exit 1
    fi

    exit 0
}

function restore() {
    echo "Restore, name=${2}"

    if [ "$1" == "copy" ]; then
        cp -r "/input/${NAME}" "/output"
    elif [ "$1"  == "archive" ]; then
        cd "/output" && tar -xf "/input/backup_${2}.tar"
    else
        echo "Invalid mode: ${1}. Expected copy or archive".
        exit 1
    fi

    exit 0
}

#
# Set default values for parameters
#
BACKUP=0
RESTORE=0
COPY=0
ARCHIVE=0
HELP=0
NAME="none"

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--backup)
        BACKUP=1
        ;;
    -r|--restore)
        RESTORE=1
        ;;
    --copy)
        COPY=1
        ;;
    --archive)
        ARCHIVE=1
        ;;
     --name)
        shift
        NAME=$1
        ;;
    -h|--help)
        HELP=1
        ;;
    *)
esac
shift # past argument or value
done


if [ "${HELP}" == "1" ]; then
    help
fi

if [ "${BACKUP}" == "1" ]; then
    if [ "${COPY}" == "1" ]; then
        backup "copy" ${NAME}
    elif [ "${ARCHIVE}" == "1" ]; then
        backup "archive" ${NAME}
    else
        echo "Falling back to default mode (archive)"
        backup "archive" ${NAME}
    fi
fi

if [ "${RESTORE}" == "1" ]; then
    if [ "${COPY}" == "1" ]; then
        restore "copy" ${NAME}
    elif [ "${ARCHIVE}" == "1" ]; then
        restore "archive" ${NAME}
    else
        echo "Falling back to default mode (archive)"
        restore "archive" ${NAME}
    fi
fi

help